FROM debian:jessie

# No Debian that's a bad Debian! We don't have an interactive prompt don't fail
ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get -y upgrade

# Install the following utilities (required by poky)
RUN apt-get install -y build-essential chrpath curl diffstat gcc-multilib gawk git-core texinfo unzip wget openssh-client tree
RUN apt-get install -y python
RUN apt-get install -y cpio

RUN apt-get clean

# Set the default shell to bash instead of dash
RUN echo "dash dash/sh boolean false" | debconf-set-selections && dpkg-reconfigure dash

# Create a non-root user that will perform the actual build
RUN useradd --uid 30000 --create-home yobuilder

ADD etc/yocto-docker.conf /etc/yocto-docker.conf

USER yobuilder
WORKDIR /home/yobuilder
